Please follow the following instructions to add the following profiles to your Cura LE software.

Material installation for M175 material profiles and print quality settings

Extract the contents of this .zip to your computer.


Windows

Navigate to the Cura LE install directory on your computer.
	-C: > Program Files (x86) > cura-lulzbot > resources

Move the extracted contents of this zip to the resources folder except for the firmware folder.

The reason for not moving over the firmware is you will want it in an easily accessible place. This is because you will need to manually choose the firmware file to update your printer within Cura

Overwrite any existing profiles and material settings.





MacOS

Terminal

/Applications/cura-lulzbot.app/Contents/Resources/cura/resources

This is the place where Cura resources are installed, we want to move them here

Using the mv command we can do this.

If you dragged the provided resources folder to your desktop
cd ~/Desktop/resources

Otherwise cd into where you dragged the folder then use the following command to transfer the files

mv * /Applications/cura-lulzbot.app/Contents/Resources/cura/resources/$FOLDER_NAME

I wouldn't recommend moving over the firmware as you will want it in an easily accessible place. This is because you will need to manually choose the firmware file to update your printer.